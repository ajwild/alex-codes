declare module 'gatsby-plugin-google-analytics'
declare module 'react-vertical-timeline-component'
declare module 'typography-breakpoint-constants'
declare const graphql: (query: TemplateStringsArray) => void
declare module '*.svg' {
  const content: any
  export default content
}
